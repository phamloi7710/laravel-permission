<?php

namespace LoiPham\LaravelPermission\Exceptions;

use InvalidArgumentException;

class WildcardPermissionNotImplementsContract extends InvalidArgumentException
{
    public static function create()
    {
        return new static('Wildcard permission class must implements LoiPham\LaravelPermission\Contracts\Wildcard contract');
    }
}
